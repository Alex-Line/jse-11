package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.api.repository.IRepository;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.exception.entity.InsertExistingEntityException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.stream.Collectors;

public final class ProjectRepository extends AbstractTMRepository<Project> implements IRepository<Project> {

    @Override
    public boolean contains(@NotNull final String entityId) {
        return false;
    }

    @NotNull
    @Override
    public Collection<Project> findAll(@NotNull final String userId) {
        @NotNull final Collection<Project> collection = new LinkedList<>();
        for (@NotNull final Map.Entry<String, Project> entry : entityMap.entrySet()) {
            if (entry.getValue().getUserId() == null) continue;
            if (entry.getValue().getUserId().equals(userId))
                collection.add(entry.getValue());
        }
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> findAllByName(
            @NotNull final String userId, @NotNull final String pattern
    ) {
        return entityMap.values().stream().filter(e ->
                ((e.getUserId() != null && e.getUserId().equals(userId)) &&
                        ((e.getName() != null && e.getName().contains(pattern))
                                || (e.getDescription() != null && e.getDescription().contains(pattern)))))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<Project> findAllByName(@NotNull final String pattern) {
        return entityMap.values().stream().filter(e ->
                ((e.getName() != null && e.getName().contains(pattern))
                        || (e.getDescription() != null && e.getDescription().contains(pattern))))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByStartDate(@NotNull final String userId) {
        return entityMap.values().stream().filter(e ->
                (e.getUserId() != null
                        && e.getUserId().equals(userId)
                        && e.getDateStart() != null))
                .sorted(Comparator.comparing(Project::getDateStart))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByStartDate() {
        return entityMap.values().stream().filter(e -> (e.getDateStart() != null))
                .sorted(Comparator.comparing(Project::getDateStart))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByFinishDate(@NotNull final String userId) {
        return entityMap.values().stream().filter(e ->
                (e.getUserId() != null
                        && e.getUserId().equals(userId)
                        && e.getDateFinish() != null))
                .sorted(Comparator.comparing(Project::getDateFinish))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByFinishDate() {
        return entityMap.values().stream().filter(e -> (e.getDateFinish() != null))
                .sorted(Comparator.comparing(Project::getDateFinish))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByStatus(@NotNull final String userId) {
        return entityMap.values().stream().filter(e ->
                (e.getUserId() != null && e.getUserId().equals(userId)))
                .sorted(Comparator.comparingInt(o -> o.getStatus().getPriority()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByStatus() {
        return entityMap.values().stream()
                .sorted(Comparator.comparingInt(o -> o.getStatus().getPriority()))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public Project findOneByName(
            @NotNull final String entityName, @NotNull final String userId
    ) {
        for (@NotNull final Map.Entry<String, Project> entry : entityMap.entrySet()) {
            if (entry.getValue().getUserId() == null) continue;
            if (entry.getValue().getName() == null) continue;
            if (!entry.getValue().getUserId().equals(userId)) continue;
            if (entry.getValue().getName().equals(entityName)) {
                return entry.getValue();
            }
        }
        return null;
    }

    @NotNull
    @Override
    public Project persist(@NotNull final Project example) throws InsertExistingEntityException {
        for (@NotNull final Map.Entry<String, Project> entry : entityMap.entrySet()) {
            if (entry.getValue().getUserId() == null) continue;
            if (entry.getValue().getName() == null) continue;
            if (!entry.getValue().getUserId().equals(example.getUserId())) continue;
            if (entry.getValue().getName().equals(example.getName()))
                throw new InsertExistingEntityException();
        }
        entityMap.put(example.getId(), example);
        return example;
    }

    @Nullable
    @Override
    public Project merge(@NotNull final Project example) {
        if (example.getUserId() == null) return null;
        if (example.getName() == null) return null;
        @Nullable final Project oldProject = findOne(example.getId());
        if (oldProject == null) return null;
        oldProject.setName(example.getName());
        oldProject.setDescription(example.getDescription());
        oldProject.setDateStart(example.getDateStart());
        oldProject.setDateFinish(example.getDateFinish());
        return oldProject;
    }

    @NotNull
    @Override
    public Collection<Project> removeAll(@NotNull final String userId) {
        @NotNull final Collection<Project> collection = new ArrayList<>();
        for (@NotNull final Iterator<Map.Entry<String, Project>> iterator = entityMap.entrySet().iterator(); iterator.hasNext(); ) {
            @NotNull final Map.Entry<String, Project> entry = iterator.next();
            if (entry.getValue().getUserId() == null) continue;
            if (entry.getValue().getUserId().equals(userId)) {
                collection.add(entry.getValue());
                iterator.remove();
            }
        }
        return collection;
    }

}