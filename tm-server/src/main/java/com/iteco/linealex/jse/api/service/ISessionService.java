package com.iteco.linealex.jse.api.service;

import com.iteco.linealex.jse.entity.Session;
import org.jetbrains.annotations.Nullable;

public interface ISessionService extends IService<Session> {

    @Nullable
    public Session getSession(
            @Nullable final String userId,
            @Nullable final String id);

    @Nullable
    public Session removeSession(@Nullable final String sessionId);

}