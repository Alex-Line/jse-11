package com.iteco.linealex.jse.api.service;

import com.iteco.linealex.jse.entity.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IProjectService extends IService<Project> {

    @NotNull
    public Collection<Project> getAllEntities(@Nullable final String userId);

    @Nullable
    public Project getEntityByName(@Nullable final String entityName);

    @Nullable
    public Project getEntityByName(
            @Nullable final String userId,
            @Nullable final String entityName);

    @NotNull
    public Collection<Project> removeAllEntities(@Nullable final String userId);

    @NotNull
    public Collection<Project> getAllEntitiesByName(@Nullable final String pattern);

    @NotNull
    public Collection<Project> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String pattern);

    @NotNull
    public Collection<Project> getAllEntitiesSortedByStartDate();

    @NotNull
    public Collection<Project> getAllEntitiesSortedByStartDate(@Nullable final String userId);

    @NotNull
    public Collection<Project> getAllEntitiesSortedByFinishDate();

    @NotNull
    public Collection<Project> getAllEntitiesSortedByFinishDate(@Nullable final String userId);

    @NotNull
    public Collection<Project> getAllEntitiesSortedByStatus();

    @NotNull
    public Collection<Project> getAllEntitiesSortedByStatus(@Nullable final String userId);

}