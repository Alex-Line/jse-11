package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.service.IProjectService;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.exception.entity.InsertExistingEntityException;
import com.iteco.linealex.jse.repository.AbstractTMRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectService extends AbstractTMService<Project> implements IProjectService {

    public ProjectService(@NotNull final AbstractTMRepository<Project> repository) {
        super(repository);
    }

    @Nullable
    @Override
    public Project persist(@Nullable final Project project) throws InsertExistingEntityException {
        if (project == null) return null;
        if (project.getName() == null || project.getName().isEmpty()) return null;
        if (project.getDescription() == null || project.getDescription().isEmpty()) return null;
        if (project.getUserId() == null || project.getUserId().isEmpty()) return null;
        return repository.persist(project);
    }

    @Nullable
    @Override
    public Project merge(@Nullable final Project entity) {
        if (entity == null) return null;
        if (entity.getName() == null) return null;
        if (entity.getUserId() == null) return null;
        @Nullable final Project existedProject = repository.findOne(entity.getId());
        if (existedProject == null) return null;
        existedProject.setDescription(entity.getDescription());
        existedProject.setDateStart(entity.getDateStart());
        existedProject.setDateFinish(entity.getDateFinish());
        return existedProject;
    }

}