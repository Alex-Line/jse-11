package com.iteco.linealex.jse.api.service;

import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.exception.TaskManagerException;
import com.iteco.linealex.jse.exception.user.LowAccessLevelException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IUserService extends IService<User> {

    @Nullable
    public User logInUser(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception;

    @Nullable
    public User createUser(
            @Nullable final User user,
            @Nullable final User selectedUser
    ) throws TaskManagerException;

    @Nullable
    public User updateUserPassword(
            @Nullable final String oldPassword,
            @Nullable final String newPassword,
            @Nullable final User selectedUser
    ) throws Exception;

    @Nullable
    public User getUser(
            @Nullable final String login,
            @Nullable final User selectedUser
    ) throws TaskManagerException;

    @NotNull
    public Collection<User> getAllUsers(
            @Nullable final User selectedUser
    ) throws LowAccessLevelException;

}