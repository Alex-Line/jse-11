package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.service.IService;
import com.iteco.linealex.jse.entity.AbstractEntity;
import com.iteco.linealex.jse.exception.entity.InsertExistingEntityException;
import com.iteco.linealex.jse.repository.AbstractRepository;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;

@NoArgsConstructor
public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    @NotNull
    protected AbstractRepository<T> repository;

    public AbstractService(@NotNull final AbstractRepository<T> repository) {
        this.repository = repository;
    }

    @Nullable
    @Override
    public T getEntityById(@Nullable String entityId) {
        if (entityId == null) return null;
        return repository.findOne(entityId);
    }

    @NotNull
    @Override
    public Collection<T> getAllEntities() {
        return repository.findAll();
    }

    @Nullable
    @Override
    public abstract T persist(@Nullable final T entity) throws InsertExistingEntityException;

    @NotNull
    @Override
    public Collection<T> persist(@NotNull final Collection<T> collection) throws InsertExistingEntityException {
        if (collection.isEmpty()) return Collections.EMPTY_LIST;
        return repository.persist(collection);
    }

    @Nullable
    @Override
    public T removeEntity(@Nullable String entityId) {
        if (entityId == null) return null;
        return repository.remove(entityId);
    }

    @NotNull
    public Collection<T> removeAllEntities(@Nullable final String userId) {
        if (userId == null) return Collections.EMPTY_LIST;
        return repository.removeAll(userId);
    }

    @NotNull
    public Collection<T> removeAllEntities() {
        return repository.removeAll();
    }

}