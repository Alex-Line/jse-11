package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.service.IPropertyService;
import com.iteco.linealex.jse.api.service.IUserService;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.enumerate.Role;
import com.iteco.linealex.jse.exception.TaskManagerException;
import com.iteco.linealex.jse.exception.entity.InsertExistingEntityException;
import com.iteco.linealex.jse.exception.user.*;
import com.iteco.linealex.jse.repository.AbstractRepository;
import com.iteco.linealex.jse.util.TransformatorToHashMD5;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull final AbstractRepository<User> repository,
                       @NotNull final IPropertyService propertyService) {
        super(repository);
        this.propertyService = propertyService;
    }

    @Nullable
    @Override
    public User logInUser(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginIncorrectException();
        if (!repository.contains(login)) throw new UserIsNotExistException();
        if (password == null || password.isEmpty()) throw new WrongPasswordException();
        @NotNull final String hashPassword = TransformatorToHashMD5.getHash(password,
                propertyService.getProperty("PASSWORD_SALT"),
                Integer.parseInt(propertyService.getProperty("PASSWORD_TIMES")));
        @Nullable final User user = repository.findOneByName(login, hashPassword);
        System.out.println(user);
        if (user != null && user.getHashPassword() != null
                && !user.getHashPassword().equals(hashPassword)) throw new WrongPasswordException();
        return user;
    }

    @Nullable
    @Override
    public User createUser(
            @Nullable final User user,
            @Nullable final User selectedUser
    ) throws TaskManagerException {
        if (selectedUser == null) return null;
        if (selectedUser.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        if (user == null) return null;
        if (user.getLogin() == null) return null;
        if (repository.contains(user.getId())) throw new InsertExistingEntityException();

        return persist(user);
    }

    @Nullable
    @Override
    public User persist(
            @Nullable final User entity
    ) throws InsertExistingEntityException {
        if (entity == null) return null;
        if (entity.getLogin().isEmpty()) return null;
        return repository.persist(entity);
    }

    @Nullable
    @Override
    public User merge(@Nullable final User entity) {
        if (entity == null) return null;
        @Nullable final User user = repository.findOne(entity.getId());
        if (user == null) return null;
        user.setLogin(entity.getLogin());
        user.setRole(entity.getRole());
        user.setHashPassword(entity.getHashPassword());
        return user;
    }

    @NotNull
    public User updateUserPassword(
            @Nullable final String oldPassword,
            @Nullable final String newPassword,
            @Nullable final User selectedUser
    ) throws Exception {
        if (selectedUser == null) throw new UserIsNotLogInException();
        if (oldPassword == null || oldPassword.isEmpty()) throw new WrongPasswordException();
        @NotNull final String hashOldPassword = TransformatorToHashMD5.getHash(oldPassword,
                propertyService.getProperty("PASSWORD_SALT"),
                Integer.parseInt(propertyService.getProperty("PASSWORD_TIMES")));
        if (selectedUser.getHashPassword() != null
                && !selectedUser.getHashPassword().equals(hashOldPassword)) throw new WrongPasswordException();
        if (newPassword == null || newPassword.isEmpty()) throw new WrongPasswordException();
        if (newPassword.length() < 8) throw new ShortPasswordException();
        @NotNull final String hashNewPassword = TransformatorToHashMD5.getHash(newPassword,
                propertyService.getProperty("PASSWORD_SALT"),
                Integer.parseInt(propertyService.getProperty("PASSWORD_TIMES")));
        selectedUser.setHashPassword(hashNewPassword);
        return selectedUser;
    }

    @NotNull
    public User getUser(
            @Nullable final String login,
            @Nullable final User selectedUser
    ) throws TaskManagerException {
        if (selectedUser == null) throw new UserIsNotLogInException();
        if (selectedUser.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        if (login == null || login.isEmpty()) throw new LoginIncorrectException();
        @Nullable final User user = repository.findOne(login);
        if (user == null) throw new UserIsNotExistException();
        return user;
    }

    @NotNull
    public Collection<User> getAllUsers(@Nullable final User selectedUser) throws LowAccessLevelException {
        if (selectedUser == null) return Collections.EMPTY_LIST;
        if (selectedUser.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        return repository.findAll();
    }

}