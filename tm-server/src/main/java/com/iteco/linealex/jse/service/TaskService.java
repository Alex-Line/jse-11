package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.service.ITaskService;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.exception.entity.InsertExistingEntityException;
import com.iteco.linealex.jse.repository.AbstractTMRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;

public final class TaskService extends AbstractTMService<Task> implements ITaskService {

    public TaskService(@NotNull final AbstractTMRepository<Task> repository) {
        super(repository);
    }

    @NotNull
    public Collection<Task> getAllEntities(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAll(userId);
    }

    @NotNull
    public Collection<Task> getAllEntities(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) return getAllEntities(userId);
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAll(projectId, userId);
    }

    @Nullable
    @Override
    public Task persist(@Nullable final Task entity) throws InsertExistingEntityException {
        if (entity == null || entity.getName() == null || entity.getName().isEmpty()) return null;
        if (entity.getUserId() == null || entity.getUserId().isEmpty()) return null;
        return repository.persist(entity);
    }

    @Nullable
    @Override
    public Task merge(@Nullable final Task entity) throws InsertExistingEntityException {
        if (entity == null || entity.getName() == null || entity.getName().isEmpty()) return null;
        if (entity.getUserId() == null || entity.getUserId().isEmpty()) return null;
        return repository.merge(entity);
    }

    @Nullable
    @Override
    public Task removeEntity(@Nullable final String entityId) {
        if (entityId == null) return null;
        return repository.remove(entityId);
    }

    @NotNull
    public Collection<Task> removeAllTasksFromProject(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) return this.removeAllEntities(userId);
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.removeAll(userId, projectId);
    }

    public boolean attachTaskToProject(
            @Nullable final String projectId,
            @Nullable final Task selectedEntity
    ) {
        if (projectId == null || projectId.isEmpty()) return false;
        if (selectedEntity == null) return false;
        for (@NotNull final Task task : repository.findAll()) {
            if (task.getProjectId() != null) continue;
            if (!task.getId().equals(selectedEntity.getId())) continue;
            task.setProjectId(projectId);
            return true;
        }
        return false;
    }

    @Nullable
    @Override
    public Task getEntityByName(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskName
    ) {
        if (userId == null) return null;
        if (projectId == null) return null;
        if (taskName == null) return null;
        return repository.findOneByName(userId, projectId, taskName);
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String pattern
    ) {
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        if (projectId == null || projectId.isEmpty()) return Collections.EMPTY_LIST;
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAllByName(pattern, projectId, userId);
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStartDate(
            @Nullable final String userId,
            @Nullable final String project
    ) {
        if (project == null) return Collections.EMPTY_LIST;
        if (userId == null) return Collections.EMPTY_LIST;
        return repository.findAllSortedByStartDate(project, userId);
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByFinishDate(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        if (projectId == null || projectId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAllSortedByFinishDate(projectId, userId);
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStatus(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        if (projectId == null || projectId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAllSortedByStatus(projectId, userId);
    }

}