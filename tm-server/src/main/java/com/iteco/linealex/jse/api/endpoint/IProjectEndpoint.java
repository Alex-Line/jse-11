package com.iteco.linealex.jse.api.endpoint;

import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.Session;
import com.iteco.linealex.jse.exception.TaskManagerException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface IProjectEndpoint {

    @Nullable
    @WebMethod
    Project getProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String entityId
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Project> getAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Project> persistProjects(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projects", partName = "projects") @NotNull final Collection<Project> collection
    ) throws TaskManagerException;

    @Nullable
    @WebMethod
    Project removeProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String entityId
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Project> removeAllProjectsByUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userID", partName = "userId") @Nullable final String userId
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Project> removeAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws TaskManagerException;

    @Nullable
    @WebMethod
    Project persistProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "project", partName = "project") @Nullable final Project entity
    ) throws TaskManagerException;

    @Nullable
    @WebMethod
    Project mergeProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "project", partName = "project") @Nullable final Project entity
    ) throws TaskManagerException;

    @Nullable
    @WebMethod
    Project getProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectName", partName = "projectName") @Nullable final String entityName
    ) throws TaskManagerException;

    @Nullable
    @WebMethod
    Project getProjectByNameWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectName", partName = "projectName") @Nullable final String entityName
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Project> getAllProjectsByNameWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Project> getAllProjectsByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Project> getAllProjectsSortedByStartDate(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Project> getAllProjectsSortedByStartDateWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Project> getAllProjectsSortedByFinishDate(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Project> getAllProjectsSortedByFinishDateWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Project> getAllProjectsSortedByStatus(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Project> getAllProjectsSortedByStatusWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Project> getAllProjectsWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws TaskManagerException;


}