package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.service.ISessionService;
import com.iteco.linealex.jse.entity.Session;
import com.iteco.linealex.jse.exception.entity.InsertExistingEntityException;
import com.iteco.linealex.jse.repository.AbstractRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class SessionService extends AbstractService<Session> implements ISessionService {

    public SessionService(@NotNull AbstractRepository<Session> repository) {
        super(repository);
    }

    @Nullable
    @Override
    public Session persist(
            @Nullable final Session entity
    ) throws InsertExistingEntityException {
        if (entity == null) return null;
        return repository.persist(entity);
    }

    @Nullable
    @Override
    public Session merge(
            @Nullable final Session entity
    ) throws InsertExistingEntityException {
        if (entity == null) return null;
        return repository.merge(entity);
    }

    @Override
    public @Nullable Session getSession(@Nullable String userId, @Nullable String Id) {
        if (Id == null) return null;
        if (userId == null) return null;
        return repository.findOne(Id);
    }

    @Override
    public @Nullable Session removeSession(@Nullable String sessionId) {
        if (sessionId == null) return null;
        return repository.remove(sessionId);
    }

}