package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.entity.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public class SessionRepository extends AbstractRepository<Session> {

    @Override
    public boolean contains(@NotNull String entityId) {
        for (Map.Entry<String, Session> entry : entityMap.entrySet()) {
            if (entry.getValue().getId().equals(entityId)) return true;
        }
        return false;
    }

    @Nullable
    @Override
    public Session remove(@NotNull final String entityName) {
        return super.remove(entityName);
    }

    @NotNull
    @Override
    public Collection<Session> findAll(@NotNull final String userId) {
        @NotNull final Collection<Session> collection = new ArrayList<>();
        for (Map.Entry<String, Session> entry : entityMap.entrySet()) {
            if (entry.getValue().getUserId() != null
                    && entry.getValue().getUserId().equals(userId))
                collection.add(entry.getValue());
        }
        return collection;
    }

}