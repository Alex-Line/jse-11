package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.entity.AbstractTMEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class AbstractTMRepository<T extends AbstractTMEntity> extends AbstractRepository<T> {

    @Nullable
    public T findOneByName(@NotNull final String entityName) {
        for (@NotNull final Map.Entry<String, T> entry : entityMap.entrySet()) {
            if (entry.getValue().getName() == null) continue;
            if (entry.getValue().getName().equals(entityName))
                return entry.getValue();
        }
        return null;
    }

    @Nullable
    public T findOneByName(
            @NotNull final String userId,
            @NotNull final String entityName
    ) {
        for (@NotNull final Map.Entry<String, T> entry : entityMap.entrySet()) {
            if (entry.getValue().getUserId() == null) continue;
            if (!entry.getValue().getUserId().equals(userId)) continue;
            if (entry.getValue().getName() == null) continue;
            if (entry.getValue().getName().equals(entityName))
                return entry.getValue();
        }
        return null;
    }

    @Nullable
    public T findOneByName(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String entityName
    ) {
        return null;
    }

    @NotNull
    public Collection<T> findAllByName(
            @NotNull final String userId,
            @NotNull final String pattern
    ) {
        return entityMap.values().stream().filter(e ->
                ((e.getUserId() != null && e.getUserId().equals(userId)) &&
                        ((e.getName() != null && e.getName().contains(pattern))
                                || (e.getDescription() != null && e.getDescription().contains(pattern)))))
                .collect(Collectors.toList());
    }

    @NotNull
    public Collection<T> findAllByName(@NotNull final String pattern) {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    public Collection<T> findAllByName(
            @NotNull final String pattern,
            @NotNull final String projectId,
            @NotNull final String userId
    ) {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    public Collection<T> findAllSortedByStartDate(
            @NotNull final String projectId,
            @NotNull final String userId
    ) {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    public Collection<T> findAllSortedByStartDate(@NotNull final String userId) {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    public Collection<T> findAllSortedByStartDate() {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    public Collection<T> findAllSortedByFinishDate(
            @NotNull final String projectId,
            @NotNull final String userId
    ) {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    public Collection<T> findAllSortedByFinishDate(@NotNull final String userId) {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    public Collection<T> findAllSortedByFinishDate() {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    public Collection<T> findAllSortedByStatus(
            @NotNull final String projectId,
            @NotNull final String userId) {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    public Collection<T> findAllSortedByStatus(@NotNull final String userId) {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    public Collection<T> findAllSortedByStatus() {
        return Collections.EMPTY_LIST;
    }

    public boolean contains(
            @NotNull final String name,
            @NotNull final String userId
    ) {
        for (@NotNull final Map.Entry<String, T> entry : entityMap.entrySet()) {
            if (entry.getValue().getUserId() == null) continue;
            if (entry.getValue().getName() == null) continue;
            if (!entry.getValue().getUserId().equals(userId)) continue;
            if (!entry.getValue().getName().equals(name)) continue;
            return true;
        }
        return false;
    }

}