package com.iteco.linealex.jse.endpoint;

import com.iteco.linealex.jse.api.endpoint.IPropertyEndpoint;
import com.iteco.linealex.jse.api.service.IPropertyService;
import com.iteco.linealex.jse.api.service.ISessionService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "com.iteco.linealex.jse.api.endpoint.IPropertyEndpoint")
public class PropertyEndpoint extends AbstractEndpoint implements IPropertyEndpoint {

    @NotNull
    private final IPropertyService propertyService;

    public PropertyEndpoint(
            @NotNull final ISessionService sessionService,
            @NotNull final IPropertyService propertyService) {
        super(sessionService, propertyService);
        this.propertyService = propertyService;
    }

    @Nullable
    @Override
    @WebMethod
    public String getProperty(
            @WebParam(name = "propertyName", partName = "propertyName") @Nullable final String propertyName) {
        return propertyService.getProperty(propertyName);
    }

}