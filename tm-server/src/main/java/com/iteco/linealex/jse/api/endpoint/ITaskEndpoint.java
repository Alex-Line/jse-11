package com.iteco.linealex.jse.api.endpoint;

import com.iteco.linealex.jse.entity.Session;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.exception.TaskManagerException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface ITaskEndpoint {

    @Nullable
    @WebMethod
    Task getTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String entityId
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Task> persistTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "tasks", partName = "tasks") @NotNull final Collection<Task> collection
    ) throws TaskManagerException;

    @Nullable
    @WebMethod
    Task removeTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String entityId
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Task> removeAllTasksWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Task> removeAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws TaskManagerException;

    @Nullable
    @WebMethod
    Task persistTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "task", partName = "task") @Nullable final Task entity
    ) throws TaskManagerException;

    @Nullable
    @WebMethod
    Task mergeTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "task", partName = "task") @Nullable final Task entity
    ) throws TaskManagerException;

    @Nullable
    @WebMethod
    Task getTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "taskName", partName = "taskName") @Nullable final String entityName
    ) throws TaskManagerException;

    @Nullable
    @WebMethod
    Task getTaskByNameWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "taskName", partName = "taskName") @Nullable final String entityName
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksByNameWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksSortedByStartDate(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksSortedByStartDateWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksSortedByFinishDate(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksSortedByFinishDateWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksSortedByStatus(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksSortedByStatusWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Task> removeAllTasksFromProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws TaskManagerException;

    @WebMethod
    boolean attachTaskToProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "task", partName = "task") @Nullable final Task selectedEntity
    ) throws TaskManagerException;

    @Nullable
    @WebMethod
    Task getTaskByNameWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskName", partName = "taskName") @Nullable final String taskName
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksByNameWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksSortedByStartDateWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksSortedByFinishDateWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws TaskManagerException;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksSortedByStatusWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws TaskManagerException;

}