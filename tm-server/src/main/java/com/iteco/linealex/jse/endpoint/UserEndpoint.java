package com.iteco.linealex.jse.endpoint;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.iteco.linealex.jse.api.endpoint.IUserEndpoint;
import com.iteco.linealex.jse.api.service.*;
import com.iteco.linealex.jse.dto.Domain;
import com.iteco.linealex.jse.entity.Session;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.exception.TaskManagerException;
import com.iteco.linealex.jse.exception.session.ThereIsNotSuchSessionException;
import com.iteco.linealex.jse.exception.system.ThereIsNotSuchFileException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.Collection;
import java.util.Collections;

@WebService(endpointInterface = "com.iteco.linealex.jse.api.endpoint.IUserEndpoint")
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    public UserEndpoint(
            @NotNull final ISessionService sessionService,
            @NotNull final IUserService userService,
            @NotNull final IPropertyService propertyService,
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService) {
        super(sessionService, propertyService);
        this.userService = userService;
        this.propertyService = propertyService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Nullable
    @Override
    @WebMethod
    public User getUserById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String entityId
    ) throws TaskManagerException {
        validateSession(session);
        return userService.getEntityById(entityId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<User> getAllUsers(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws TaskManagerException {
        validateSession(session);
        return userService.getAllEntities();
    }

    @Nullable
    @Override
    @WebMethod
    public User persistUser(
            @WebParam(name = "user", partName = "user") @Nullable final User entity
    ) throws TaskManagerException {
        return userService.persist(entity);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<User> persistUsers(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "users", partName = "users") @NotNull final Collection<User> collection
    ) throws TaskManagerException {
        validateSession(session);
        return userService.persist(collection);
    }

    @Nullable
    @Override
    @WebMethod
    public User removeUserById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws TaskManagerException {
        validateSession(session);
        return userService.removeEntity(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<User> removeAllUsersWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws TaskManagerException {
        validateSession(session);
        return userService.removeAllEntities();
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<User> removeAllUsers(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws TaskManagerException {
        validateSession(session);
        return userService.removeAllEntities();
    }

    @Nullable
    @Override
    @WebMethod
    public User logInUser(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) throws Exception {
        return userService.logInUser(login, password);
    }

    @Nullable
    @Override
    @WebMethod
    public void logOutUser(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception {
        if(session == null) throw new ThereIsNotSuchSessionException();
        getSessionService().removeEntity(session.getId());
    }

    @Nullable
    @Override
    @WebMethod
    public User createUser(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "user", partName = "user") @Nullable final User user,
            @WebParam(name = "selectedUser", partName = "selectedUser") @Nullable final User selectedUser
    ) throws TaskManagerException {
        validateSession(session);
        return userService.createUser(user, selectedUser);
    }

    @Nullable
    @Override
    @WebMethod
    public User mergeUser(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "user", partName = "user") @Nullable final User entity
    ) throws TaskManagerException {
        validateSession(session);
        return null;
    }

    @Nullable
    @Override
    @WebMethod
    public User updateUserPassword(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "oldPassword", partName = "oldPassword") @Nullable final String oldPassword,
            @WebParam(name = "newPassword", partName = "newPassword") @Nullable final String newPassword,
            @WebParam(name = "selectedUser", partName = "selectedUser") @Nullable final User selectedUser
    ) throws Exception {
        validateSession(session);
        return userService.updateUserPassword(oldPassword, newPassword, selectedUser);
    }

    @Nullable
    @Override
    @WebMethod
    public User getUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "selectedUser", partName = "selectedUser") @Nullable final User selectedUser
    ) throws TaskManagerException {
        validateSession(session);
        return userService.getUser(login, selectedUser);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<User> getAllUsersBySelectedUser(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "selectedUser", partName = "selectedUser") @Nullable final User selectedUser
    ) throws TaskManagerException {
        validateSession(session);
        return userService.getAllUsers(selectedUser);
    }

    @Override
    @WebMethod
    public void loadBinary(@Nullable final Session session) throws Exception {
        validateSession(session);
        @NotNull final File savedFile = new File(propertyService.getProperty("SAVE_PATH") + "data.bin");
        if (!savedFile.exists()) throw new ThereIsNotSuchFileException();
        @NotNull final FileInputStream inputStream = new FileInputStream(savedFile);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        if (domain.getUsers() == null) userService.persist(Collections.EMPTY_LIST);
        else userService.persist(domain.getUsers());
        if (domain.getProjects() == null) projectService.persist(Collections.EMPTY_LIST);
        else projectService.persist(domain.getProjects());
        if (domain.getTasks() == null) taskService.persist(Collections.EMPTY_LIST);
        else taskService.persist(domain.getTasks());
    }

    @Override
    @WebMethod
    public void loadFasterJson(@Nullable final Session session) throws Exception {
        validateSession(session);
        @NotNull final File savedFile = new File(propertyService.getProperty("SAVE_PATH") + "data.json");
        if (!savedFile.exists()) throw new ThereIsNotSuchFileException();
        @NotNull final XmlMapper mapper = new XmlMapper();
        @NotNull final Domain domain = mapper.readValue(savedFile, Domain.class);
        if (domain.getUsers() == null) userService.persist(Collections.EMPTY_LIST);
        else userService.persist(domain.getUsers());
        if (domain.getProjects() == null) projectService.persist(Collections.EMPTY_LIST);
        else projectService.persist(domain.getProjects());
        if (domain.getTasks() == null) taskService.persist(Collections.EMPTY_LIST);
        else taskService.persist(domain.getTasks());
    }

    @Override
    @WebMethod
    public void loadFasterXml(@Nullable final Session session) throws Exception {
        validateSession(session);
        @NotNull final File savedFile = new File(propertyService.getProperty("SAVE_PATH") + "data.xml");
        if (!savedFile.exists()) throw new ThereIsNotSuchFileException();
        @NotNull final XmlMapper mapper = new XmlMapper();
        @NotNull final Domain domain = mapper.readValue(savedFile, Domain.class);
        if (domain.getUsers() == null) userService.persist(Collections.EMPTY_LIST);
        else userService.persist(domain.getUsers());
        if (domain.getProjects() == null) projectService.persist(Collections.EMPTY_LIST);
        else projectService.persist(domain.getProjects());
        if (domain.getTasks() == null) taskService.persist(Collections.EMPTY_LIST);
        else taskService.persist(domain.getTasks());
    }

    @Override
    @WebMethod
    public void loadJaxbJson(@Nullable final Session session) throws Exception {
        validateSession(session);
        @NotNull final File savedFile = new File(propertyService.getProperty("SAVE_PATH") + "data.json");
        if (!savedFile.exists()) throw new ThereIsNotSuchFileException();
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        System.out.println(System.getProperty("java.classpath"));
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(savedFile);
        if (domain.getUsers() == null) userService.persist(Collections.EMPTY_LIST);
        else userService.persist(domain.getUsers());
        if (domain.getProjects() == null) projectService.persist(Collections.EMPTY_LIST);
        else projectService.persist(domain.getProjects());
        if (domain.getTasks() == null) taskService.persist(Collections.EMPTY_LIST);
        else taskService.persist(domain.getTasks());
    }

    @Override
    @WebMethod
    public void loadJaxbXml(@Nullable final Session session) throws Exception {
        validateSession(session);
        @NotNull final File savedFile = new File(propertyService.getProperty("SAVE_PATH") + "data.xml");
        if (!savedFile.exists()) throw new ThereIsNotSuchFileException();
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(savedFile);
        if (domain.getUsers() == null) userService.persist(Collections.EMPTY_LIST);
        else userService.persist(domain.getUsers());
        if (domain.getProjects() == null) projectService.persist(Collections.EMPTY_LIST);
        else projectService.persist(domain.getProjects());
        if (domain.getTasks() == null) taskService.persist(Collections.EMPTY_LIST);
        else taskService.persist(domain.getTasks());
    }

    @Override
    @WebMethod
    public void saveBinary(@Nullable final Session session) throws Exception {
        validateSession(session);
        @NotNull final Domain domain = new Domain();
        domain.load(userService, projectService, taskService);
        @Nullable final String savePath = propertyService.getProperty("SAVE_PATH");
        if (savePath == null) throw new ThereIsNotSuchFileException();
        @NotNull final File saveDir = new File(savePath);
        saveDir.mkdirs();
        @NotNull final File saveFile = new File(savePath + "data.bin");
        @NotNull final FileOutputStream outputStream = new FileOutputStream(saveFile);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.flush();
        objectOutputStream.close();
    }

    @Override
    @WebMethod
    public void saveFasterJson(@Nullable final Session session) throws Exception {
        validateSession(session);
        @NotNull final Domain domain = new Domain();
        domain.load(userService, projectService, taskService);
        @Nullable final String savePath = propertyService.getProperty("SAVE_PATH");
        if (savePath == null) throw new ThereIsNotSuchFileException();
        @NotNull final File saveDir = new File(savePath);
        saveDir.mkdirs();
        @NotNull final File saveFile = new File(savePath + "data.json");
        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(saveFile, domain);
    }

    @Override
    @WebMethod
    public void saveFasterXml(@Nullable final Session session) throws Exception {
        validateSession(session);
        @NotNull final Domain domain = new Domain();
        domain.load(userService, projectService, taskService);
        @Nullable final String savePath = propertyService.getProperty("SAVE_PATH");
        if (savePath == null) throw new ThereIsNotSuchFileException();
        @NotNull final File saveDir = new File(savePath);
        saveDir.mkdirs();
        @NotNull final File saveFile = new File(savePath + "data.xml");
        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(saveFile, domain);
    }

    @Override
    @WebMethod
    public void saveJaxbJson(@Nullable final Session session) throws Exception {
        validateSession(session);
        @NotNull final Domain domain = new Domain();
        domain.load(userService, projectService, taskService);
        @Nullable final String savePath = propertyService.getProperty("SAVE_PATH");
        if (savePath == null) throw new ThereIsNotSuchFileException();
        @NotNull final File saveDir = new File(savePath);
        saveDir.mkdirs();
        @NotNull final File saveFile = new File(savePath + "data.json");
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, saveFile);
    }

    @Override
    @WebMethod
    public void saveJaxbXml(@Nullable Session session) throws Exception {
        validateSession(session);
        @NotNull final Domain domain = new Domain();
        domain.load(userService, projectService, taskService);
        @Nullable final String savePath = propertyService.getProperty("SAVE_PATH");
        if (savePath == null) throw new ThereIsNotSuchFileException();
        @NotNull final File saveDir = new File(savePath);
        saveDir.mkdirs();
        @NotNull final File saveFile = new File(savePath + "data.xml");
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, saveFile);
    }

}