package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.api.repository.IRepository;
import com.iteco.linealex.jse.entity.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public final class UserRepository extends AbstractRepository<User> implements IRepository<User> {

    public boolean contains(@NotNull final String entityId) {
        for (@NotNull final Map.Entry<String, User> entry : entityMap.entrySet()) {
            if (!entry.getValue().getLogin().equals(entityId)) continue;
            return true;
        }
        return false;
    }

    @Nullable
    public User findOneByName(
            @NotNull final String login,
            @NotNull final String hashPassword
    ) {
        for (Map.Entry<String, User> entry : entityMap.entrySet()) {
            if(!entry.getValue().getLogin().equals(login)) continue;
            if(entry.getValue().getHashPassword().equals(hashPassword)) return entry.getValue();
        }
        return null;
    }

    @NotNull
    @Override
    public Collection<User> findAll(@NotNull final String userId) {
        @NotNull final Collection<User> collection = new ArrayList<>();
        if (userId.isEmpty()) return collection;
        for (@NotNull final Map.Entry<String, User> entry : entityMap.entrySet()) {
            if (entry.getValue().getId().equals(userId))
                collection.add(entry.getValue());
        }
        return collection;
    }

    @Nullable
    @Override
    public User merge(@NotNull final User example) {
        @Nullable final User user = findOne(example.getId());
        if (user == null) return null;
        user.setRole(example.getRole());
        user.setHashPassword(example.getHashPassword());
        return user;
    }

}