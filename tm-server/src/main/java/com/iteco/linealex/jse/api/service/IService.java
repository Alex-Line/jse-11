package com.iteco.linealex.jse.api.service;

import com.iteco.linealex.jse.exception.entity.InsertExistingEntityException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IService<T> {

    @Nullable
    public T persist(@Nullable final T entity) throws InsertExistingEntityException;

    @NotNull
    public Collection<T> persist(@NotNull final Collection<T> collection) throws InsertExistingEntityException;

    @Nullable
    public T merge(@Nullable final T entity) throws InsertExistingEntityException;

    @Nullable
    public T getEntityById(@Nullable final String entityId);

    @NotNull
    public Collection<T> getAllEntities();

    @Nullable
    public T removeEntity(@Nullable final String entityId);

    @NotNull
    public Collection<T> removeAllEntities();

}