package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.entity.AbstractTMEntity;
import com.iteco.linealex.jse.exception.entity.InsertExistingEntityException;
import com.iteco.linealex.jse.repository.AbstractTMRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;

public class AbstractTMService<T extends AbstractTMEntity> extends AbstractService<T> {

    @NotNull
    protected AbstractTMRepository<T> repository;

    public AbstractTMService(@NotNull AbstractTMRepository<T> repository) {
        this.repository = repository;
    }

    @Nullable
    @Override
    public T persist(@Nullable T entity) throws InsertExistingEntityException {
        if (entity == null) return null;
        return repository.persist(entity);
    }

    @Nullable
    @Override
    public T merge(@Nullable T entity) throws InsertExistingEntityException {
        if (entity == null) return null;
        return repository.merge(entity);
    }

    @Nullable
    public T getEntityByName(@Nullable final String entityName) {
        if (entityName == null) return null;
        return repository.findOneByName(entityName);
    }

    @Nullable
    public T getEntityByName(
            @Nullable final String userId,
            @Nullable final String entityName
    ) {
        if (entityName == null || entityName.isEmpty()) return null;
        if (userId == null) return null;
        return repository.findOneByName(entityName, userId);
    }

    @NotNull
    public Collection<T> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String pattern
    ) {
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAllByName(userId, pattern);
    }

    @NotNull
    public Collection<T> getAllEntitiesByName(@Nullable final String pattern) {
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAllByName(pattern);
    }

    @NotNull
    public Collection<T> getAllEntitiesSortedByStartDate() {
        return repository.findAllSortedByStartDate();
    }

    @NotNull
    public Collection<T> getAllEntitiesSortedByStartDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAllSortedByStartDate(userId);
    }

    @NotNull
    public Collection<T> getAllEntitiesSortedByFinishDate() {
        return repository.findAllSortedByFinishDate();
    }

    @NotNull
    public Collection<T> getAllEntitiesSortedByFinishDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAllSortedByFinishDate(userId);
    }

    @NotNull
    public Collection<T> getAllEntitiesSortedByStatus() {
        return repository.findAllSortedByStatus();
    }

    @NotNull
    public Collection<T> getAllEntitiesSortedByStatus(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAllSortedByStatus(userId);
    }

    @NotNull
    public Collection<T> getAllEntities(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAll(userId);
    }

}