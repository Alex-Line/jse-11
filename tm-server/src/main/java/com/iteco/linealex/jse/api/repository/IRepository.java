package com.iteco.linealex.jse.api.repository;

import com.iteco.linealex.jse.exception.entity.InsertExistingEntityException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.security.NoSuchAlgorithmException;
import java.util.Collection;

public interface IRepository<T> {

    public boolean contains(@NotNull final String entityId);

    @NotNull
    public Collection<T> findAll();

    @Nullable
    public T persist(@NotNull final T example) throws InsertExistingEntityException, NoSuchAlgorithmException;

    @NotNull
    public Collection<T> persist(@NotNull final Collection<T> collection);

    @Nullable
    public T merge(@NotNull final T example) throws InsertExistingEntityException;

    @Nullable
    public T remove(@NotNull final String name);

    @NotNull
    public Collection<T> removeAll();

}