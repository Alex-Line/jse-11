package com.iteco.linealex.jse.api.service;

import com.iteco.linealex.jse.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ITaskService extends IService<Task> {

    @NotNull
    Collection<Task> removeAllTasksFromProject(
            @Nullable final String userId,
            @Nullable final String projectId);

    @NotNull
    public Collection<Task> removeAllEntities(@Nullable final String userId);

    public boolean attachTaskToProject(
            @Nullable final String projectId,
            @Nullable final Task selectedEntity);

    @Nullable
    public Task getEntityByName(@NotNull final String taskName);

    @Nullable
    public Task getEntityByName(
            @Nullable final String userId,
            @Nullable final String taskName);

    @Nullable
    public Task getEntityByName(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskName);

    @NotNull
    public Collection<Task> getAllEntities(
            @Nullable final String userId);

    @NotNull
    public Collection<Task> getAllEntities(
            @Nullable final String userId,
            @Nullable final String projectId);

    @NotNull
    public Collection<Task> getAllEntitiesByName(@Nullable final String pattern);

    @NotNull
    public Collection<Task> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String pattern);

    @NotNull
    public Collection<Task> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String pattern);

    @NotNull
    public Collection<Task> getAllEntitiesSortedByStartDate();

    @NotNull
    public Collection<Task> getAllEntitiesSortedByStartDate(@Nullable final String userId);

    @NotNull
    public Collection<Task> getAllEntitiesSortedByStartDate(
            @Nullable final String userId,
            @Nullable final String project);

    @NotNull
    public Collection<Task> getAllEntitiesSortedByFinishDate();

    @NotNull
    public Collection<Task> getAllEntitiesSortedByFinishDate(@Nullable final String userId);

    @NotNull
    public Collection<Task> getAllEntitiesSortedByFinishDate(
            @Nullable final String userId,
            @Nullable final String projectId);

    @NotNull
    public Collection<Task> getAllEntitiesSortedByStatus();

    @NotNull
    public Collection<Task> getAllEntitiesSortedByStatus(@Nullable final String userId);

    @NotNull
    public Collection<Task> getAllEntitiesSortedByStatus(
            @Nullable final String userId,
            @Nullable final String projectId);

}