package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.service.IPropertyService;
import com.iteco.linealex.jse.repository.PropertyRepository;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;

@Getter
@Setter
public class PropertyService implements IPropertyService {

    @NotNull
    private final PropertyRepository properties = new PropertyRepository();

    public PropertyService() {
        try {
            properties.init();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public String getProperty(@Nullable final String propertyName) {
        if (propertyName == null) return null;
        return properties.getProperties().getProperty(propertyName);
    }

}