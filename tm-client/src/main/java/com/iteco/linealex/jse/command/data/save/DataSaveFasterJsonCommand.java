package com.iteco.linealex.jse.command.data.save;

import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

public class DataSaveFasterJsonCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data-save-faster-json";
    }

    @NotNull
    @Override
    public String description() {
        return "SAVE DATA INTO JSON FILE BY FASTER-XML";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("SAVING DATA TO JSON FILE");
        serviceLocator.getUserEndpoint().saveFasterJson(serviceLocator.getSession());
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}