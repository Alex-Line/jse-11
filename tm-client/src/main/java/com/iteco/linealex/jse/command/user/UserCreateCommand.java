package com.iteco.linealex.jse.command.user;

import com.iteco.linealex.jse.api.endpoint.TaskManagerException_Exception;
import com.iteco.linealex.jse.api.endpoint.User;
import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.util.TransformatorToHashMD5;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-create";
    }

    @NotNull
    @Override
    public String description() {
        return "REGISTER NEW USER IF THEY DO NOT EXIST. (AVAILABLE FOR AMDMINS ONLY)";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER USER LOGIN. IT MUST BE UNIQUE!");
        @NotNull final String login = serviceLocator.getTerminalService().nextLine();
        System.out.println("ENTER USER PASSWORD. IT MUST BE 8 DIGITS OR LONGER");
        @NotNull final String password = serviceLocator.getTerminalService().nextLine();
        if (password.length() < 8) throw new TaskManagerException_Exception();
        @NotNull final String hashPassword = TransformatorToHashMD5.getHash(password,
                serviceLocator.getPropertyEndpoint().getProperty("PASSWORD_SALT"),
                Integer.parseInt(serviceLocator.getPropertyEndpoint().getProperty("PASSWORD_TIMES")));
        @Nullable final User newUser = new User();
        newUser.setLogin(login);
        newUser.setHashPassword(hashPassword);
        System.out.println("[WAS REGISTERED NEW USER]");
        System.out.println(newUser);
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}