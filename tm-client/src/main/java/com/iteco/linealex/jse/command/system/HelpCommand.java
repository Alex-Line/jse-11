package com.iteco.linealex.jse.command.system;

import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "SHOW ALL AVAILABLE COMMANDS";
    }

    @Override
    public void execute() throws Exception {
        for (final Map.Entry<String, AbstractCommand> entry : serviceLocator.getCommands().entrySet()) {
            System.out.println(entry.getValue().command() + " : " + entry.getValue().description());
        }
    }

    @Override
    public boolean secure() {
        return false;
    }

}